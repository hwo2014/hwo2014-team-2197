if USE_OPENGL then
	require 'glut'
end
require 'Font'
require 'TrackPiece'

Track = {
	wingAngle = math.pi/3,
}

Track.__index = Track

function Track:new(data)
	local self = {
		piece={},
		lastInfo={},
		segment={}
	}
	setmetatable(self, Track)

	local start = data.startingPoint
	local x = start.position.x
	local y = start.position.y
	local a = math.rad(start.angle-90)

	local lanes = data.lanes
	self.lanes = lanes
	for i,info in ipairs(data.pieces) do
		local piece = TrackPiece:new(x, y , a, info, lanes)
		self.segment[i] = {}
		x, y, a = piece:map01(1)
		self.piece[i] = piece
	end

	self.smallFont = Font:new()
	self.smallFont.width = 10
	self.bigFont = Font:new()

	return self
end

function Track:SegmentUntilSwitch(currentPiece, laneIndex)
	local segment = self.segment[currentPiece+1][laneIndex]
	if segment then
		return segment
	end
	segment = {
		left = {
			sum = 0,
			minRadius = 0,
			maxRadius = 0,
		},
		right = {
			sum = 0,
			minRadius = 0,
			maxRadius = 0,
		},
		curvePieces = 0
	}
	local pieceCount = #self.piece
	for i=currentPiece+1, pieceCount+currentPiece do
		local piece = self:getPiece(i)
		if piece.switch and segment.curvePieces>0 then
			break
		end
		local radians = piece.radians
		if radians then
			segment.curvePieces = segment.curvePieces + 1
			local radius = piece.radius
			local turn
			if radians > 0 then
				turn = segment.right
				radius = radius - piece.laneDistance[laneIndex]
			else
				turn = segment.left
				radius = radius + piece.laneDistance[laneIndex]
			end
			local length = math.abs(radius*radians)
			turn.sum = turn.sum + length
			if radius < turn.minRadius then
				turn.minRadius = radius
			end
			if radius > turn.maxRadius then
				turn.maxRadius = radius
			end
		end
		table.insert(segment, piece)
	end

	self.segment[currentPiece+1][laneIndex] = segment
	return segment
end

function Track:getPiece(serverIndex)
	return self.piece[serverIndex%#self.piece+1]
end

function Track:radiusAt(pieceIndex, laneIndex)
	local piece = self:getPiece(pieceIndex)
	return piece:radiusAt(laneIndex)
end

function Track:lookAhead(meters, car)
	local segment = {
		left = {
			sum = 0,
			minRadius = 0,
			maxRadius = 0,
		},
		right = {
			sum = 0,
			minRadius = 0,
			maxRadius = 0,
		},
		curvePieces = {

		},
		switches = {

		},
		cars = { --TODO

		},
	}

	--TODO current piece data


	local position = car.carPosition
	local i = position.pieceIndex
	local distance = - position.inPieceDistance
	while distance < meters do
		local piece = self:getPiece(i)
		if piece.switch then
			local switch = {}
			switch.distance = distance
			table.insert(segment.switches, switch)
		end

		local radians = piece.radians
		if radians then
			table.insert(segment.curvePieces, piece)
			local radius = piece:radiusAt(laneIndex) --TODO after switch use smaller
			local turn
			if radians > 0 then
				turn = segment.right
			else
				turn = segment.left
			end
			local length = math.abs(radius*radians)
			turn.sum = turn.sum + length
			if radius < turn.minRadius then
				turn.minRadius = radius
			end
			if radius > turn.maxRadius then
				turn.maxRadius = radius
			end
		end
		table.insert(segment, piece)
		distance = distance + piece.length
		i = i+1
	end
	return segment
end


return Track