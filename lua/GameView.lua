require 'opengl'
require 'glut'

local GameView = {
  width  = 1024,
  height = 768,

  ctrl_c = 3,

  debug = {
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            log={
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
            }
          }
}

function GameView:display()
  gl.Clear("COLOR_BUFFER_BIT,DEPTH_BUFFER_BIT")

  gl.Color(0.0, 0.0, 1.0)

  gl.Begin("LINES")
  gl.Vertex(self.pos1.x, self.pos1.y)
  gl.Vertex(self.pos2.x, self.pos2.y)
  gl.End()

  --gl.Flush()
  glut.SwapBuffers()
end

function GameView:reshape(w, h)
  w = w or self.width
  h = h or self.height
  gl.Viewport(0, 0, w, h)
  gl.LoadIdentity()
  gl.Ortho(-w*1.2, w*1.2, h*1.2, -h*1.2, -1.0, 1.0)
  self.width  = w
  self.height = h
end

function GameView:keyboard(key, x, y)
  if key == self.ctrl_c then
    os.exit()
  end

  print(key,x,y)
end

function GameView:setpositions(data)
  gl.Clear("COLOR_BUFFER_BIT,DEPTH_BUFFER_BIT")
  self:draw()

  for _,car in ipairs(data) do
    gl.Color(0,0,0)
    self:drawCar(car)
  end

  glut.SwapBuffers()
end

function GameView:draw()
  local track = self.track
  for i,piece in ipairs(track.piece) do
    if piece.radians then
      gl.Color(0.9,math.random(),math.random())
    else
      gl.Color(math.random(),0.9,math.random())
    end
    gl.Color(1,1,1,1)

    gl.Color(unpack(piece.color))

    gl.LineWidth(1)
    for l=0, #track.lanes-1 do
        gl.Begin("LINE_STRIP")
      if piece.radius then
        gl.Vertex(piece.x, piece.y)
        for t=0, 1, 1/4 do
          local x, y = piece:map01(t,l)
          gl.Vertex(x, y)
        end
        gl.Vertex(piece.x, piece.y)
        else
        local x, y = piece:map01(0,l)
        gl.Vertex(x, y)
        x, y = piece:map01(1,l)
        gl.Vertex(x, y)
      end
        gl.End()
    end
  end

  --Draw labels
  for i,piece in ipairs(track.piece) do
    if piece.switch then
      gl.LineWidth(1)
      gl.Color(unpack(piece.color))
        gl.Begin("LINES")
        local x, y
      for l=0, #track.lanes-2 do
        x, y = piece:map01(0,l)
        gl.Vertex(x, y)
        x, y = piece:map01(1,(l+1)%#track.lanes)
        gl.Vertex(x, y)

        x, y = piece:map01(1,l)
        gl.Vertex(x, y)
        x, y = piece:map01(0,(l+1)%#track.lanes)
        gl.Vertex(x, y)
      end
        gl.End()
    end
    if piece.radius then
      local radius = math.floor(piece.radius)
      track.smallFont:drawWithStroke(piece.x, piece.y, radius, piece.color)
    end
  end
end

function GameView:drawUI(speed)
  Font:drawWithStroke(0,-300,self.speed or 0, {0.8,0.8,0.8},3)
  Font:drawWithStroke(250,-300,self.throttle or 0, {0.8,0.8,0.8},3)

  local color = {0.8,0.8,0.8}
  for i,line in ipairs(self.debug) do
    for j,number in ipairs(line) do
      previous = self.debug.log[i][j]
      if previous then
        if previous > number then
          color = {0.8,0,0}
        elseif previous < number then
          color = {0,0.8,0}
        end
      end
      Font:drawWithStroke(300+j*200, -1024+i*130, number or 0, color, 3)
      self.debug.log[i][j] = number
    end
  end
--[[
  local track = self.track
  local last = track.lastInfo[car.id.color]
  if last then
    local dx
    if last.piecePosition.pieceIndex == car.piecePosition.pieceIndex then
      dx = car.piecePosition.inPieceDistance - last.piecePosition.inPieceDistance
    else
      local lastLength = self.track.piece[last.piecePosition.pieceIndex+1]
      dx = lastLength.length - last.piecePosition.inPieceDistance
      dx = dx + car.piecePosition.inPieceDistance
    end
    car.speed = dx*10
    Font:drawWithStroke(0,-200,car.speed, {0.8,0.8,0.8})
  end
  track.lastInfo[car.id.color] = car
--]]
end

function GameView:drawCar(car)
  local track = self.track
  self:drawUI()

  local position = car.piecePosition
  local drift = math.rad(car.angle)

  local trackPiece = track.piece[position.pieceIndex+1]
  local x,y,a = trackPiece:map(position.inPieceDistance, position.lane.endLaneIndex)
  gl.Color(1,1,1)
  a = a + drift
  local wingAngle = math.pi + a + track.wingAngle

  local carSize = 42

  local color = 1-drift/track.wingAngle
  
  gl.Color(1,color,color,0.5)
  Font:drawWithStroke(x,y,car.angle, {1,color,color},2)

  gl.Begin("LINE_STRIP")
  gl.Vertex(x - math.cos(a)*carSize, y - math.sin(a)*carSize)
  gl.Vertex(x,y)
  gl.Vertex(x + math.cos(wingAngle)*carSize*0.6, y + math.sin(wingAngle)*carSize*0.6)
  gl.End()

  wingAngle = math.pi + a - track.wingAngle
  gl.Begin("LINE_STRIP")
  gl.Vertex(x - math.cos(a)*carSize, y - math.sin(a)*carSize)
  gl.Vertex(x,y)
  gl.Vertex(x + math.cos(wingAngle)*carSize*0.6, y + math.sin(wingAngle)*carSize*0.6)
  gl.End()

end

function GameView:init(track)
  self.track = track

  glut.InitWindowSize(GameView.width, GameView.height)
  glut.Init(arg)
  glut.InitDisplayMode()
  glut.CreateWindow(arg[0])
  gl.ClearColor(0.0, 0.0, 0.0, 1.0)

  GameView:reshape(GameView.width,GameView.height)

  GLUT_DISPLAY_FUNC = function(...)GameView:display(...)end
  GLUT_RESHAPE_FUNC = function(...)GameView:reshape(...)end
  GLUT_KEYBOARD_FUNC = function(...)GameView:keyboard(...)end
  GLUT_IDLE_FUNC = function(...)GameView:timer(...)end
  glut.KeyboardFunc("GLUT_KEYBOARD_FUNC");
  glut.ReshapeFunc("GLUT_RESHAPE_FUNC");
  glut.DisplayFunc("GLUT_DISPLAY_FUNC");
  glut.IdleFunc("GLUT_IDLE_FUNC");

  --glut.MainLoop()
end

function GameView:SaveScreenshot()
  local pixels = {}
  for x=0, 100 do
    for y=0, 100 do
      local p = gl.ReadPixels(x,y,2,2,"RGB")
      print(p,#pixels,pixels[1])
      --NOT WORKING
    end
  end
end

return GameView