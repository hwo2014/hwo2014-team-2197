if USE_OPENGL then
	require 'glut'
end

Font = {
	width = 42,
	LineWidth = 1.5,
}

Font.__index = Font

function Font:new()
	local self = {}
	setmetatable(self, Font)
	return self
end

function Font:drawWithStroke(x, y, number, color, decimal, leadingZeroes)
	leadingZeroes = leadingZeroes or 0

	local inverseColor = {1, 1, 1}
	for i,v in ipairs(color) do
		if v>0.5 then
			inverseColor = {0, 0, 0}
			break
		end
	end

	if type(number)~="number" then
		decimal = 0
	end

	if decimal and decimal>0 then
		local l = self.width
		self.width = self.width/2
		local n = (number - math.floor(number))*10^decimal
		if n>0 then
			self:drawWithStroke(x+l*decimal,y,math.floor(n),color,0,decimal)
		end
		self.width = l
	end

	gl.Color(unpack(inverseColor))
	gl.LineWidth(self.LineWidth+40)
	self:draw(x, y, number, leadingZeroes)

	gl.Color(unpack(color))
	gl.LineWidth(self.LineWidth)
	self:draw(x, y, number, leadingZeroes)

end

function Font:draw(x, y, number, leadingZeroes)
	decimal = decimal or 3

	local a,b,c,d,e,f,g = true,true,false,false,true,false,true
	local l = self.width


	if type(number)=="number" and number~=math.huge then
		number = math.abs(number)
		local n = math.floor(number%10)

		a = n==0 or n==2 or n==3 or n==5 or n==7 or n==8 or n==9
		b = n==0 or n==1 or n==2 or n==3 or n==4 or n==7 or n==8 or n==9
		c = n==0 or n==1 or n==3 or n==4 or n==5 or n==6 or n==7 or n==8 or n==9
		d = n==0 or n==2 or n==3 or n==5 or n==6 or n==8
		e = n==0 or n==2 or n==6 or n==8
		f = n==0 or n==4 or n==5 or n==6 or n==8 or n==9
		g = n==2 or n==3 or n==4 or n==5 or n==6 or n==8 or n==9

		if number>9  or leadingZeroes>0 then
			self:draw(x-l*1.5,y,math.floor(number/10),0,leadingZeroes-1)
		end
	else
		if number=="c" then
			a,b,c,d,e,f,g = true,false,false,true,true,true,false
		elseif number=="s" then
			a,b,c,d,e,f,g = true,false,true,true,false,true,true
		elseif number=="a" then
			a,b,c,d,e,f,g = true,true,true,false,true,true,true
		end
	end

	gl.Begin("LINES")
	if a then
		gl.Vertex(x+0,y+0,0)
		gl.Vertex(x+l,y+0,0)
	end

	if b then
		gl.Vertex(x+l,y+0,0)
		gl.Vertex(x+l,y+l,0)
	end

	if c then
		gl.Vertex(x+l,y+l,0)
		gl.Vertex(x+l,y+l+l,0)
	end

	if d then
		gl.Vertex(x+l,y+l+l,0)
		gl.Vertex(x+0,y+l+l,0)
	end

	if e then
		gl.Vertex(x+0,y+l+l,0)
		gl.Vertex(x+0,y+l,0)
	end

	if f then
		gl.Vertex(x+0,y+l,0)
		gl.Vertex(x+0,y+0,0)
	end

	if g then
		gl.Vertex(x+0,y+l,0)
		gl.Vertex(x+l,y+l,0)
	end
	gl.End()


end