TrackPiece = {
	colors = {
		index = 1,
		{0,0,1},
		{0,1,0},
		{0,1,1},
		{1,0,0},
		{1,0,1},
		{1,1,0},
		{0.5,0.5,1},
		{0.5,1,0.5},
		{0.5,1,1},
		{1,0.5,0.5},
		{1,0.5,1},
		{1,1,0.5},
		{0.5,0,1},
		{0.5,1,0},
		{1,0.5,0},
		{0,0.5,1},
		{0,1,0.5},
		{1,0,0.5},
		{1,1,1},
	},
}

TrackPiece.__index = TrackPiece

function TrackPiece:new(x, y, angle, info, lanes)
	local self = {startAngle = angle}
	setmetatable(self, TrackPiece)

	for k,v in pairs(info) do
	end

	self.switch = info.switch

	self.laneDistance = {}
	for i,lane in ipairs(lanes) do
		self.laneDistance[lane.index] = lane.distanceFromCenter
	end

	if info.length then
		self.length = info.length

		self.x = x
		self.y = y
	else
		self.radius = info.radius
		self.degrees = info.angle
		self.radians = math.rad(info.angle)
		self.length = math.abs(self.radius * self.radians)

		if self.radians < 0 then
			self.baseAngle = angle + math.pi/2
		else
			self.baseAngle = angle - math.pi/2
		end

		self.x = x + self.radius * math.cos(self.baseAngle + math.pi)
		self.y = y + self.radius * math.sin(self.baseAngle + math.pi)
	end

	if TrackPiece.lastPiece then
		if TrackPiece.lastPiece.radius then
			if TrackPiece.lastPiece.radius == self.radius then
				self.color = TrackPiece.lastPiece.color
			end
		elseif not self.radius then
			self.color = TrackPiece.lastPiece.color
		end
	end
	if not self.color then
		self.color = self.colors[self.colors.index]
		self.colors.index = (self.colors.index % #self.colors) + 1
	end

	TrackPiece.lastPiece = self

	return self
end

function TrackPiece:map01(f, laneIndex)
	local length = self.length
	return self:map(f*length, laneIndex)
end

function TrackPiece:map(position, laneIndex)
	local x = 0
	local y = 0
	
	local f = position/self.length
	local angle

	local radius = self.radius
	if radius and laneIndex then
		local laneDistance = self.laneDistance[laneIndex]
		if self.radians > 0 then
			laneDistance = -laneDistance
		end
		radius = radius + laneDistance
	end
	if self.radians then
		angle = self.baseAngle + f*self.radians
		x = radius * math.cos(angle)
		y = radius * math.sin(angle)

		if self.radians>0 then 
			angle = angle + math.pi/2
		else
			angle = angle - math.pi/2
		end
	else
		angle = self.startAngle
		x = self.length * f * math.cos(angle)
		y = self.length * f * math.sin(angle)

		if laneIndex then
			local laneDistance = self.laneDistance[laneIndex]
			x = x + laneDistance * math.cos(angle+math.pi/2)
			y = y + laneDistance * math.sin(angle+math.pi/2)
		end
		angle = angle
	end

	return self.x+x, self.y+y, angle
end

function TrackPiece:radiusAt(laneIndex)
	local radius = self.radius
	if not radius then
		return 100 --TODO remove
	end

	if self.radians > 0 then
		radius = radius - self.laneDistance[laneIndex]
	else
		radius = radius + self.laneDistance[laneIndex]
	end

	return radius
end