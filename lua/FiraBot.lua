require "Track"

if USE_OPENGL then
   gameView = require "GameView"
else
   gameView = {
      debug = {
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
         {},
      }
   }
end

local dummyFile = {}
function dummyFile:write()end
function dummyFile:open()end
function dummyFile:close()end
function dummyFile:flush()end
function dummyFile:read()end

local function LOGD(msg, params)
   if DEBUG then
      --print(string.format(msg, params))
      io.write(params..'\n')
   end
end

local FiraBot = {
   allowTurbo = true
}
FiraBot.__index = FiraBot

function FiraBot:new(conn, name, key)
   local bot = {
      speed = 0,
      angle = 0,
      deltaAngle = 0,
      odometer = 0,
      gameTick = 0,
      throttle = -1,
      enginePower = 10,
      friction = 100,
      turbo = {
         lastTick = -1,
      }
   }
   setmetatable(bot, FiraBot)
   bot.conn = conn
   bot.name = name
   bot.key = key
   if DEBUG then
      bot.racelog = io.open("./graph/"..name..".tsv","w")
      bot.racelog:write("tick\todometer\tspeed\tangle\tdelta angle\tradius\n")
      bot.speedlog = io.open("./prediction/speed_"..name..".tsv","w")
      bot.speedlog:write("predicted\tactual\tdiff\n")
   else
      bot.racelog = dummyFile
      bot.speedlog = dummyFile
   end

   bot.curvelogname = "./curve/curve_"..name
   bot.curvelogindex = 0

   if DEBUG then
      io.output("/tmp/log/"..name..".log")
   end

   TICK_PREDICTION = TICK_PREDICTION or 10
   bot.speedPrediction = {
      speed = 0,
      tick = 0
   }

   return bot
end

function FiraBot:nextAngleFunc(speed,radius,angle,friction,readValue)
   if not radius then
      return angle/2
   end
   local angle = speed*speed/radius*math.sin(math.rad(angle))
   local newFriction
   if readValue then
      newFriction = readValue/angle
   end
   return angle*self.friction, newFriction
end

function FiraBot:msg(msgtype, data)
   return json.encode.encode({msgType = msgtype, data = data})
end

function FiraBot:send(msg)
   LOGD("Sending msg: %s", msg)
   self.conn:send(msg .. "\n")
end

function FiraBot:join()
   return self:msg("join", {name = self.name, key = self.key})
end

function FiraBot:create(trackName, carCount, password)
   local data = {}
   data.botId = {name = self.name, key = self.key}
   data.trackName = trackName
   data.carCount = carCount or 1
   data.password = password
   return self:msg("createRace", data)
end

function FiraBot:joinRace(trackName, carCount, password)
   local data = {}
   data.botId = {name = self.name, key = self.key}
   data.trackName = trackName
   data.carCount = carCount or 1
   data.password = password
   return self:msg("joinRace", data)
end

--[[
function FiraBot:throttle(throttle)
   return self:msg("throttle", throttle)
end
--]]

function FiraBot:throttleWithEngine(targetSpeed)
   local throttle = 1

   if targetSpeed<0.3 then targetSpeed = 0.3 end

   if self.speed > targetSpeed+0.1 then
      throttle = 0
   elseif self.speed < targetSpeed-0.1 then
      throttle = 1
   else
      throttle = targetSpeed/self.enginePower
      if self.turbo.lastTick > self.gameTick then
         throttle = throttle / self.turbo.data.turboFactor
      end
   end

--[[
   if self.angle > 0 then
      if self.speed < targetSpeed and self.angle < 5 then
         throttle = targetSpeed/self.enginePower
      end
   ended
   --]]

   if throttle > 1 then
      throttle = 1
   elseif throttle < 0 then
      throttle = 0
   end

   self.throttle = throttle
   gameView.throttle = throttle
   self:send(self:msg("throttle", throttle))
end

function FiraBot:breakTestThrottle()
   if self.speed > 9 then
      self.breaking = true
   elseif self.speed < 1 then
      self.breaking = false
   end
   local throttle = self.breaking and 0.0 or 1.0
   throttle = 0.4
   self.throttle = throttle
   gameView.throttle = throttle
   if self.throttle~=throttle then
      self:send(self:msg("throttle", throttle))
   end
end

function FiraBot:throttleToSpeed(speed)
   local throttle = 1
   
   if false and self.currentPiece.radius then
      if not self.currentPiece.entranceThrottle then
         self.currentPiece.entranceThrottle = self.speed/self.enginePower
      end
      throttle = self.currentPiece.entranceThrottle
   elseif self.fixedThrottle then
      throttle = self.fixedThrottle
   elseif self.fixedSpeed then
      --[[
      local predicted = self.speed
      gameView.debug[6][1] = self.fixedSpeed-0.1
      gameView.debug[6][2] = self.fixedSpeed
      gameView.debug[6][3] = self.fixedSpeed+0.1
      if predicted >= self.fixedSpeed+0.1 then
         throttle = 0
      elseif predicted <= self.fixedSpeed-0.1 then
         throttle = 1
      end
      ]]
   elseif self.fixedSpeed then
      local predicted = self.speed
      if predicted >= self.fixedSpeed then
         self.fixedThrottle = self.fixedSpeed/enginePower
      else
         for i=0.5,0.9,0.1 do
            if predicted >= self.fixedSpeed*(1+i) then
               throttle = self.fixedSpeed*(1-i)*0.1
            end
         end
      end
   else
      if self.angle and math.abs(self.angle)>42 then
         throttle = 0
      elseif not speed or self.speed < speed then
         throttle = 1
         if self.allowTurbo then
            self:useTurbo()
         end
      else
         throttle = speed
      end
      if self.turbo.lastTick and self.gameTick and self.turbo.lastTick > self.gameTick then
         throttle = throttle / self.turbo.data.turboFactor
      end
   end


   if self.throttle~=throttle then
      self:send(self:msg("throttle", throttle))
   end
   self.throttle = throttle
   gameView.throttle = throttle
end

function FiraBot:ping()
   return self:msg("ping", {})
end

function FiraBot:run(trackName)
   if trackName then
      self.trackName = trackName
      self:send(self:create(trackName))
   else
      self:send(self:join())
   end
   self:msgloop()
end

function FiraBot:onjoin(data)
   print("Joined")
   self:send(self:ping())
end

function FiraBot:ongameinit(data)
   self.track = Track:new(data.race.track)
   if USE_OPENGL then
      gameView:init(self.track)
   end
end

function FiraBot:ongamestart(data)
   print("Race started")
   self:send(self:msg("throttle",self.throttle))
end

function FiraBot:oncarpositions(data)
   self.currentPosition = data[1]
   local piecePosition = data[1].piecePosition
   local pieceIndex = piecePosition.pieceIndex

   if self.currentPiece then
      self.currentPiece.entranceThrottle = nil
   end

   self.currentPiece = self.track:getPiece(pieceIndex)
   self.currentPiece.index = pieceIndex

   self.lane = piecePosition.lane.startLaneIndex
   local lane = self.lane

   local newAngle = data[1].angle
   self.deltaAngle = newAngle-self.angle
   if not self.crashed and self.currentPiece.radius and self.speed > 1 and self.angle > 30 then
      _, self.friction = self:nextAngleFunc(self.speed,self.currentPiece.radius,self.angle,self.friction, newAngle)
   end
   self.angle = data[1].angle

   local _,_,tangle = self.currentPiece:map(piecePosition.inPieceDistance,self.lane)
   tangle = math.deg(tangle)
   self.deltaTangent = tangle - (self.angleToTangent or 0)
   self.angleToTangent = tangle
   
   if math.abs(self.angle)>1 then
      --self:logAngle()
   end


   --todo choose this car
   self:updateCurrentSpeed(piecePosition)
   
   local r1 = self.track:getPiece(pieceIndex+1).radius
   local r2 = self.track:getPiece(pieceIndex+2).radius
   local r3 = self.track:getPiece(pieceIndex+3).radius
   local targetSpeed = 9.3
   
   --[[
   if r3 and r3 < 80 then
      targetSpeed = 7.5
   elseif r2 and r2 < 80 then
      targetSpeed = 5.5
   elseif r1 and r1 < 150 then
      targetSpeed = 4.5
   end
   --]]
   --self:throttleToSpeed(targetSpeed)
   --self:breakTestThrottle()

   targetSpeed = self:simulateCrash(self.enginePower)

   self:throttleWithEngine(targetSpeed)
   
   local segment = self.track:SegmentUntilSwitch(piecePosition.pieceIndex, lane)
   if segment.left.sum > segment.right.sum then
      self:send(self:msg("switchLane", "Left"))
   else
      self:send(self:msg("switchLane", "Right"))
   end
   if USE_OPENGL then
      gameView:setpositions(data)
   end

   self:logRace(piecePosition)
   self:logCurves()


   self:send(self:ping())

   self.speedlog:write(""..self.odometer.."\t"..self.speed.."\t"..self.angle.."\t"..self.enginePower.."\n")

   self:luiggilog()
   self:logFriction()
end

function FiraBot:logFriction()
   if DEBUG then
      if not self.frictionLog then

         self.frictionLog = io.open("./friction/flog"..".tsv","w")
         self.frictionLog:write("angle\t")
         self.frictionLog:write("radius\t")
         self.frictionLog:write("dangle\tangle\t")
         self.frictionLog:write("friction\t")
         self.frictionLog:write("calculated\n")
      end
      local calculated = self:nextAngleFunc(self.speed,self.currentPiece.radius,self.angle,self.friction)
      self.frictionLog:write(""..self.odometer.."\t")
      self.frictionLog:write(""..(self.currentPiece.radius or 0).."\t")
      self.frictionLog:write(""..self.deltaAngle.."\t"..self.angle.."\t")
      self.frictionLog:write(""..self.friction.."\t")
      self.frictionLog:write(""..calculated.."\n")
   end
end

function FiraBot:simulateCrash(speed)
   local position = {
      angle = self.currentPosition.angle,   
      piecePosition = {
         pieceIndex = self.currentPosition.piecePosition.pieceIndex,
         inPieceDistance = self.currentPosition.piecePosition.inPieceDistance,
         lap = self.currentPosition.piecePosition.lap,
         --not using lane yet
      }
   }

   local simOdometer = 0
   function move(speed)
      local piece = self.track:getPiece(position.piecePosition.pieceIndex)
      position.piecePosition.inPieceDistance = position.piecePosition.inPieceDistance + speed
      simOdometer = simOdometer + speed
      local remainingPieceLength = position.piecePosition.inPieceDistance - piece.length
      if 0 > remainingPieceLength then
         position.piecePosition.pieceIndex = position.piecePosition.pieceIndex + 1 --TODO reset
         position.piecePosition.inPieceDistance = -remainingPieceLength
      end
      piece = self.track:getPiece(position.piecePosition.pieceIndex)

      if piece.radius then
         position.angle = self:nextAngleFunc(self.speed,piece.radius,self.angle,newAngle)
         --print(position.angle, math.cos(math.rad(position.angle)))
      end
   end

   for i=0,50 do
      move(speed)
      if simOdometer > 200 then
         break
      end
      if position.angle > 51 then
         if speed > 1 then
            return self:simulateCrash(speed-1)
         else
            return 1
         end
      end
   end

   gameView.debug[2][1] = speed
   gameView.debug[2][2] = "x"
   gameView.debug[2][3] = simOdometer
   gameView.debug[3][1] = self.friction
   gameView.debug[3][2] = 1
   gameView.debug[3][3] = position.angle

   return speed
end

function FiraBot:luiggilog()
   if DEBUG then
      local angle = self.deltaTangent
      local rad = math.rad(angle)
      local v = self.speed
      local r = self.currentPiece.radius or 0--self.track:radiusAt(self.currentPiece.index,self.lane) or 1
      if not self.luiggilogfile then
         self.luiggilogfile = io.open("./luiggilog"..".tsv","w")
         self.luiggilogfile:write("odometer\t")
         self.luiggilogfile:write("radius\t")
         self.luiggilogfile:write("deltaTangent\tangle\tsin(deltaTangent)\tcos(deltaTangent)\t")
         self.luiggilogfile:write("0.5*v^2*sin(deltaTangent)\tv^2(-sin(deltaTangent)\t0.5*v^2\t")
         self.luiggilogfile:write("v^2/r\tv^2/r*cos(deltaTangent)\tspeed\tdeltaangle\ttangentsum\n")
      end
      self.luiggilogfile:write(""..self.odometer.."\t")
      self.luiggilogfile:write(""..(self.currentPiece.radius or 0).."\t")
      self.luiggilogfile:write(""..angle.."\t"..self.angle.."\t"..math.sin(rad).."\t"..math.cos(rad).."\t")
      self.luiggilogfile:write(""..(0.5*v^2*math.sin(rad)).."\t"..(v^2*(-math.sin(rad)).."\t"..(0.5*v^2).."\t"))
      self.luiggilogfile:write(""..(v^2/r).."\t"..(v^2/r*math.cos(rad)).."\t"..self.speed.."\t"..self.deltaAngle.."\t"..self.tangentSum.."\n")
   end
end
function FiraBot:logCurves()
   if not DEBUG then
      return
   end
   local radius = self.currentPiece.radius or 0--self.track:radiusAt(self.currentPiece.index,self.lane) or 1
   local v2r = self.speed*self.speed/radius
   if self.currentPiece.radius then
      if not self.currentPiece.entranceAngle then
         self.currentPiece.entranceAngle = self.angle
      end

      if self.curvelog then
         if self.curvelogradius and self.curvelogradius~=self.currentPiece.radius then
            self.curvelog:flush()
            self.curvelog:close()
            self.curvelog = nil
            self.curvelogradius = nil
         end
      end
      self.curvelogradius = self.currentPiece.radius
      if self.angle > 0 then
         if not self.curvelog then
            self.curvelog = io.open(""..self.curvelogname.."_"..self.curvelogindex.."_"..radius..".tsv","w")
            self.curvelog:write("speed\tangle\tradius\tposition\tpieceindex\tV^2/r\tcos(deltaAngle)\tsin(deltaAngle)\n")
            self.curvelogindex = self.curvelogindex + 1
            self.curvestart = self.odometer
         end
         if self.speed>0 then
            local length = self.odometer - self.curvestart
            v2r = v2r^0.5
            local rad = math.rad(self.deltaAngle)
            self.curvelog:write(""..self.speed.."\t"..math.abs(self.angle).."\t"..radius.."\t"..length.."\t"..self.currentPiece.index.."\t"..v2r.."\t"..math.cos(rad).."\t"..math.sin(rad).."\n")
            self.curvelog:flush()
         else
            self.curvelog:flush()
            self.curvelog:close()
            self.curvelog = nil
            self.curvelogradius = nil
         end
      end
   else
      if self.curvelog then
         self.curvelog:flush()
         self.curvelog:close()
         self.curvelog = nil
         self.curvelogradius = nil
      end
   end

   if DEBUG then
      --[[
      if self.deltaTangent==0 and math.abs(self.angle)>0 then
         if not self.tangentzerolog then
            self.tangentzerolog = io.open(""..self.curvelogname.."_".."Straight"..".tsv","w")
            self.tangentzerolog:write("speed\tangle\tradius\tposition\tpieceindex\tV^2/r\tcos(deltaAngle)\tsin(deltaAngle)\n")
         end
         self.tangentzerolog:write(""..self.speed.."\t"..math.abs(self.angle).."\t"..math.abs(self.deltaAngle).."\n")
      else
      --]]
      if self.deltaTangent>0 then
         if not self.tangenttestentranceangle then
            self.tangenttestentranceangle = self.angle
         end
         self.dangleSum = self.dangleSum + self.deltaAngle
         self.tangentSum = self.tangentSum + self.deltaTangent
         if not self.tangentdeltalog then
            self.tangentdeltalog = io.open(""..self.curvelogname.."_".."tangentdelta"..".tsv","w")
            self.tangentdeltalog:write("radius\tspeed\tdeltaTangente\tspeed/pieceradius\tangle\tdeltaangle\tdeltaanglesum\ttangentsum\tentranceangle\tcurvedegrees\n")
         end
         self.tangentdeltalog:write(""..radius.."\t"..self.speed.."\t"..self.deltaTangent.."\t"..math.deg(self.speed/radius).."\t"..self.angle.."\t"..self.deltaAngle.."\t"..self.dangleSum.."\t"..self.tangentSum.."\t"..self.tangenttestentranceangle.."\t"..(self.currentPiece.degrees or 0).."\n")
      else
         self.dangleSum = 0
         self.tangentSum = 0
         self.tangenttestentranceangle = nil
         self.tangenttestcurrentpiece = self.pieceIndex
      end
   end

   gameView.debug[1][1] = "a"
   gameView.debug[1][2] = "c"
   gameView.debug[1][3] = "s"
--[[
   gameView.debug[2][1] = self.angle
   gameView.debug[2][2] = math.cos(math.rad(self.angle))
   gameView.debug[2][3] = math.sin(math.rad(self.angle))
   gameView.debug[3][1] = self.deltaAngle
   gameView.debug[3][2] = math.cos(math.rad(self.deltaAngle))
   gameView.debug[3][3] = math.sin(math.rad(self.deltaAngle))
   gameView.debug[4][1] = self.deltaTangent
   gameView.debug[4][2] = math.cos(math.rad(self.deltaTangent))
   gameView.debug[4][3] = math.sin(math.rad(self.deltaTangent))
   gameView.debug[5][1] = v2r
   gameView.debug[5][2] = math.cos(math.rad(self.angle))/v2r
   gameView.debug[5][3] = math.sin(math.rad(self.angle))/v2r
   --]]
end
function FiraBot:logRace(piecePosition)
   local piece = self.track:getPiece(piecePosition.pieceIndex)
   local radius = 0
   if piece.radians then
      radius = self.track:radiusAt(piecePosition.pieceIndex,piecePosition.lane.startLaneIndex)
   end
   self.racelog:write(""..self.gameTick.."\t"..self.odometer.."\t"..self.speed.."\t"..math.abs(self.angle).."\t"..math.abs(self.deltaAngle).."\t"..(radius/10).."\n")
end

function FiraBot:logAngle()
   if not DEBUG then
      return
   end
   local last = self.lastPosition
   if last then
      local piece = self.track:getPiece(last.pieceIndex)
      local anglelog = io.open("angle"..self.track:radiusAt(last.pieceIndex,last.lane.startLaneIndex)..".tsv","a")
      anglelog:write(""..self.fixedThrottle.."\t"..math.abs(self.speed).."\t"..self.deltaAngle.."\t"..math.abs(self.angle).."\n")
      anglelog:close()
   end
end

function FiraBot:updateCurrentSpeed(currentPosition)
   local track = self.track
   local last = self.lastPosition
   if last then
      local dx
      if last.pieceIndex == currentPosition.pieceIndex then
         dx = currentPosition.inPieceDistance - last.inPieceDistance
      else
         local lastPiece = self.track:getPiece(last.pieceIndex)
         local radians = lastPiece.radians
         local lastLength = 0
         if radians then
            lastLength = math.abs(lastPiece.radians) * lastPiece:radiusAt(currentPosition.lane.endLaneIndex)
         else
            lastLength = lastPiece.length
         end
         dx = lastLength - last.inPieceDistance
         dx = dx + currentPosition.inPieceDistance
      end
      self:predictSpeedAndLog(dx)
      self:adjustEnginePower(dx)
      self.speed = dx
      --self.speed = self:predictSpeed(1)
      self.odometer = self.odometer + dx
      gameView.speed = self.speed

   end
   self.lastPosition = {}
   for k,v in pairs(currentPosition) do
      self.lastPosition[k] = v
   end
end

function FiraBot:predictSpeed(ticks)
   local throttle = self.throttle or 0
   --[[
   if self.turbo.lastTick > self.gameTick then
      throttle = throttle * self.turbo.data.turboFactor
   end
   --]]
   local prediction = self.speed + 0.020408*ticks*(self.enginePower*throttle-self.speed)
   return prediction
end

function FiraBot:predictSpeedAndLog(readValue)
   local prediction = self:predictSpeed(1)
   if self.gameTick>5 or readValue>0.1 then
      --self.speedlog:write(""..prediction.."\t"..readValue.."\t"..prediction-readValue.."\n")
   end
   self.speedlog:flush()
   return prediction
end

function FiraBot:adjustEnginePower(readValue)
   local prediction = self:predictSpeed(1)
   local diff = prediction-readValue
   if diff < 0 then
      self.enginePower = self.enginePower + 0.1
   else
      self.enginePower = self.enginePower - 0.1
   end
end

function FiraBot:predictSpeedInTicks(readValue)
   if self.gameTick >= self.speedPrediction.tick then
      if readValue>0 then
         if self.skippedFirst then
            self.speedlog:write(""..self.speedPrediction.speed.."\t"..readValue.."\t"..self.speedPrediction.speed-readValue.."\n")
            self.speedlog:flush()
         else
            self.skippedFirst = true
         end
      end
      TICK_PREDICTION = TICK_PREDICTION + 10
      self.speedPrediction = {
         speed = self:predictSpeed(TICK_PREDICTION),
         tick = self.gameTick + TICK_PREDICTION,
      }
   end
end

--[[{
   "turboDurationMilliseconds": 500.0,
   "turboDurationTicks": 30,
   "turboFactor": 3.0
}]]
function FiraBot:onturboavailable(data)
   self.turbo.available = true
   self.turbo.data = data
end

function FiraBot:useTurbo(data)
   if not self.turbo.available then
      return
   else
      return
   end
   self.turbo.lastTick = self.gameTick + self.turbo.data.turboDurationTicks
   self.turbo.available = false
   self:send(self:msg("turbo", "Fira in the hole!"))
end

function FiraBot:oncrash(data)
   self.crashed = true

   if not DEBUG then
      return
   end
   local last = self.lastPosition
   if last then
      local crashlog = self.crashlog or io.open("crash.txt","a")
      local piece = self.track:getPiece(last.pieceIndex)
      radius = piece.radius and self.track:radiusAt(last.pieceIndex, last.lane.startLaneIndex) or 0
      local direction = "Straight"
      if piece.radians then
         direction = piece.radians > 0 and "Right" or "Left"
      end
      --crashlog:write(self.trackName.."\t"..self.speed.."\t"..(self.fixedThrottle or "throttle not fixed").."\t"..math.abs(self.angle).."\t"..math.abs(self.deltaAngle).."\t"..radius.."\t"..direction.."\n")

      crashlog:close()
      self:send(self:ping())
   end


   if self.curvelog then
      self.curvelog:flush()
      self.curvelog:close()
      self.curvelog = nil
   end
end

function FiraBot:ongameend(data)
   print("Race ended")
   self:send(self:ping())
end

function FiraBot:onerror(data)
   print("Error: " .. data)
   self:send(self:ping())
end

function FiraBot:ontournamentend()
   os.exit()
end

function FiraBot:onspawn()
   self.crashed = false
end

function FiraBot:processLine(line)
   local msg = json.decode.decode(line)
   if msg then
      local msgtype = msg["msgType"]
      self.gameTick = msg.gameTick or self.gameTick
      if self.crashskip and self.crashed and msgtype~="spawn" then
         return
      end
      local fn = FiraBot["on" .. string.lower(msgtype)]
      if fn then
         fn(self, msg["data"])
      else
         print("Got " .. msgtype)
         self:send(self:ping())
      end
   end
end

function FiraBot:msgloop()
   local line = self.conn:receive("*l")
   while true do
      if line and #line>0 then
         LOGD("Got message: %s", line)
         self:processLine(line)
      end
      line = self.conn:receive("*l")
   end
end

return FiraBot