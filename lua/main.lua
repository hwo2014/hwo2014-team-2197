DEBUG = false
USE_OPENGL = false

local socket = require "socket"
local json = require "json"
--local posix = require "posix"
local FiraBot = require "FiraBot"

function createBot(host, port, name, key)
	local c = assert(socket.connect(host, port))
	return FiraBot:new(c, name, key)
end

if #arg == 4 then
	local host, port, name, key = unpack(arg)
	print("Connecting with parameters:")
	print(string.format("host=%s, port=%s, bot name=%s, key=%s", unpack(arg)))

	local tracks = {
		--"keimola",
		--"germany",
		"usa",
		--"france",
	}

	--
	local botCount = 1
	if botCount == 1 then
		createBot(host, port, name, key):run()
	else
		math.randomseed(os.time())
		local servername = "fira"..math.floor(math.random()*(10^3))
		local server = createBot(host, port, servername.."s", key)
		server:send(server:create("usa", botCount, "fira"))
		local pid = posix.fork()
		if pid==0 then
			server:msgloop()
			os.exit()
		end
		for i=1,botCount do
			pid = posix.fork()
			if pid==0 then
				local bot = createBot(host, port, name..i, key)
				bot:send(server:joinRace("usa", botCount, "fira"))
				bot:msgloop()
			end
		end
	end
	--]]

	--[[Multiple tests
	for i=1.0,1.0,0.01 do
		for _,track in ipairs(tracks) do
			local pid = posix.fork()
			if pid==0 then
			local botname = track.."_"..i
			if FiraBot.allowTurbo then
				botname = botname.."_turbo"
			end
			--local bot = createBot(host, port, botname, key)
			local bot = createBot(host, port, name, key)
			--bot.fixedSpeed = i*10
			bot:run(track)
			os.exit()
			end
		end
	end
	--]]
else
	print("Usage: ./run host port botname botkey")
end
