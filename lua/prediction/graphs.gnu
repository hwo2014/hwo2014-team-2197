set encoding utf8						#Allowing us to use UTF-8 Characters
set   autoscale                         # scale axes automatically
unset log                               # remove any log-scaling
unset label                             # remove any previous labels
set xtic auto	                        # set xtics automatically
set ytic auto                           # set ytics automatically
set pointsize 1
set border 3 front linetype -1 linewidth 1.000
set boxwidth 0.8 absolute
set grid nopolar
#set grid noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
#set grid layerdefault linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

# Plotting Graph 1
set datafile missing '-'
#set terminal postscript eps color linewidth 1
set term png enhanced size 2048,2048
#set xlabel  offset character -1, -1, -1 font ",14" textcolor lt 0 norotate
set title font ",12"

###### INFLUÊNCIA DO TAMANHO DO ESPECTRO

#set xrange [300:500]
#set yrange [-0.30:0.3]
#set xtics 200
#set ytics 1

france(n) = sprintf("speed_france_0.%d.tsv", n)
usa(n) = sprintf("speed_usa_0.%d.tsv", n)
germany(n) = sprintf("speed_germany_0.%d.tsv", n)
keimola(n) = sprintf("speed_keimola_0.%d.tsv", n)

#set key below vertical maxrows 2
#set key top left
#set xlabel "erro"
#set ylabel "error"
set output 'predicition.eps'
plot 	\
		for [i=1:9] germany(i)		using ($2) title "" with lines lw 1 lt rgb "#0000FF",\
		for [i=1:9] keimola(i)		using ($2) title "" with lines lw 1 lt rgb "#FF00FF",\
		for [i=1:99] france(i)		using ($2) title "" with lines lw 1 lt rgb "#FF0000",\
		for [i=1:99] usa(i)		using ($2) title "" with lines lw 1 lt rgb "#00FF00",\
		for [i=1:9] germany(i)		using ($1) title "" with lines lw 1 lt rgb "#000088",\
		for [i=1:9] keimola(i)		using ($1) title "" with lines lw 1 lt rgb "#FF0088",\
		for [i=1:99] france(i)		using ($1) title "" with lines lw 1 lt rgb "#FF0088",\
		for [i=1:99] usa(i)		using ($1) title "" with lines lw 1 lt rgb "#0000FF",\
		for [i=1:9] germany(i)		using ($3) title "" with lines lw 1 lt rgb "#000088",\
		for [i=1:9] keimola(i)		using ($3) title "" with lines lw 1 lt rgb "#FF0088",\
		for [i=1:99] france(i)		using ($3) title "" with lines lw 1 lt rgb "#FF0088",\
		for [i=1:99] usa(i)		using ($3) title "" with lines lw 1 lt rgb "#FF0000",\


#set xrange [200:2000]
#set yrange [5:7]
set output 'usa.eps'
plot 	\
		for [i=1:19] france(i)		using ($1):($2) title "" with lines,\
		#for [i=1:19] france(i)		using ($1):($4/2) title "" with lines,\
		#for [i=1:19] france(i)		using ($1):($3*0.1) title "" with lines,\
		#for [i=1:99] france(i)		using ($1) title "" with lines,\

#set xrange [825:2000]
#set yrange [5:7]
set output 'engine.eps'

#set palette defined (0 0 0 0, 1 0 0 1)

plot 	\
		for [i=1:9] france(i)		using ($1):($3) title "" with lines,\
		for [i=1:9] france(i)		using ($1):($2) title "" with lines,\
		#for [i=1:9] france(i)		using ($1):($4*0.4) title "" with lines,\
		#for [i=1:99] france(i)		using ($1) title "" with lines,\


