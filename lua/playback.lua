USE_OPENGL = true
DEBUG = false


local json = require "json"
local gameView = require "GameView"
local Track = require "Track"
local FiraBot = require "FiraBot"

local bot = FiraBot:new(nil,"play","back")

function nop()end

bot.send = nop
bot.msg = nop
bot.throttle = nop
bot.crashskip = true

function read()
	local line = io.read()
	while line and #line > 0 do
		--print(line)
		bot:processLine(line)
		line = io.read()
	end
end

read()